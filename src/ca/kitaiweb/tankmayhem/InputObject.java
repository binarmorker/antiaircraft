package ca.kitaiweb.tankmayhem;

import java.util.concurrent.ArrayBlockingQueue;
import android.view.MotionEvent;

/**
 * A simplified input object to detect touch events and motion.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class InputObject {
	public static final byte EVENT_TYPE_KEY = 1;
	public static final byte EVENT_TYPE_TOUCH = 2;
	public static final int ACTION_KEY_DOWN = 1;
	public static final int ACTION_KEY_UP = 2;
	public static final int ACTION_TOUCH_DOWN = 3;
	public static final int ACTION_TOUCH_MOVE = 4;
	public static final int ACTION_TOUCH_UP = 5;
	public ArrayBlockingQueue<InputObject> pool;
	public byte eventType;
	public long time;
	public int action;
	public int keyCode;
	public int x;
	public int y;

	/**
	 * The constructor for the input object.
	 * 
	 * @param pool The pool of inputs
	 */
	public InputObject(ArrayBlockingQueue<InputObject> pool) {
		this.pool = pool;
	}

	/**
	 * Consume the event input.
	 * 
	 * @param event The gesture used
	 */
	public void useEvent(MotionEvent event) {
		eventType = EVENT_TYPE_TOUCH;
		int a = event.getAction();
		switch (a) {
		case MotionEvent.ACTION_DOWN:
			action = ACTION_TOUCH_DOWN;
			break;
		case MotionEvent.ACTION_MOVE:
			action = ACTION_TOUCH_MOVE;
			break;
		case MotionEvent.ACTION_UP:
			action = ACTION_TOUCH_UP;
			break;
		default:
			action = 0;
		}
		time = event.getEventTime();
		x = (int) event.getX() ;
		y = (int) event.getY();
	}

	/**
	 * Check which input was made at a given time.
	 * 
	 * @param event The gesture used
	 * @param historyItem The item searched
	 */
	public void useEventHistory(MotionEvent event, int historyItem) {
		eventType = EVENT_TYPE_TOUCH;
		action = ACTION_TOUCH_MOVE;
		time = event.getHistoricalEventTime(historyItem);
		x = (int) event.getHistoricalX(historyItem);
		y = (int) event.getHistoricalY(historyItem);
	}

	/**
	 * Add the input object to the events pool.
	 */
	public void returnToPool() {
		pool.add(this);
	}
}