package ca.kitaiweb.tankmayhem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;

import ca.kitaiweb.tankmayhem.R;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;

/**
 * The main thread in which the game runs.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class GameThread extends Thread {
	private SurfaceHolder mSurfaceHolder;
	public DisplayMetrics metrics = new DisplayMetrics();
	private static Bitmap mBackgroundImage;
	public static Bitmap tankImg;
	public static Bitmap cannonImg;
	public static Bitmap ufoImg;
	public static boolean mRun = false;
	public static boolean mPaused = false;
	private boolean isFiring;
	private Tank tank;
	public static Resources res;
	private Object mPauseLock;
	private Object inputQueueMutex = new Object();
	private ArrayBlockingQueue<InputObject> inputQueue = new ArrayBlockingQueue<InputObject>(30);
	private static ArrayList<Projectile> bullets;
	private static ArrayList<Ufo> ufos;
	private long lastUpdateTime;
	private long newUfoTimer;
	private Hud hud;

	/**
	 * Constructor for the thread, which initializes everything.
	 * 
	 * @param surfaceHolder The surface on which to draw
	 * @param context The Android context
	 * @param handler The handler for communications between the thread and the activity
	 */
	public GameThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
		mPauseLock = new Object();
		mSurfaceHolder = surfaceHolder;
		Resources res = context.getResources();
		mBackgroundImage = BitmapFactory.decodeResource(res,R.drawable.background);
		tankImg = BitmapFactory.decodeResource(res,R.drawable.tank);
		cannonImg = BitmapFactory.decodeResource(res,R.drawable.cannon);
		ufoImg= BitmapFactory.decodeResource(res,R.drawable.ufo);
		bullets = new ArrayList<Projectile>();
		ufos = new ArrayList<Ufo>();
		SoundManager.loadSound(context);
	}

	@Override
	public void run() {	
		synchronized (mPauseLock) {
            while (mPaused) {
                try {
                    mPauseLock.wait();
                } catch (InterruptedException e) {
                }
            }
        }
		
		while (mRun) {
			
			Canvas c = null;
			try {
				c = mSurfaceHolder.lockCanvas(null);
				synchronized (mSurfaceHolder) {
					long currentTime = System.currentTimeMillis();
					long delta = (long) (currentTime - lastUpdateTime);
					lastUpdateTime = currentTime; 
					processInput();
					updatePhysics(delta);
					SoundManager.update(delta);
					if (mRun) {
						draw(c);
					}
				}
			} finally {
				if (c != null) {
					mSurfaceHolder.unlockCanvasAndPost(c);
				}
			}
		}
	}
	
	/**
	 * Launched when the game is put on pause.
	 */
	public void onPause() {
		SoundManager.pauseMusic();
		synchronized (mPauseLock) {
            mPaused = true;
        }
	}
	
	/**
	 * Launched when the game is resumed from pause.
	 */
	public void onResume() {
		SoundManager.resumeMusic();
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }
	
	/**
	 * Change the running status of the game.
	 * 
	 * @param b If the game should be running or not
	 */
	public void setRunning(boolean b) {
		mRun = b;
	}
	
	/**
	 * Draw the game on screen.
	 * 
	 * @param canvas The canvas on which to draw the game
	 */
	private void draw(Canvas canvas) {
		canvas.drawBitmap(mBackgroundImage, 0, 0, null);
		for(Iterator<Projectile> it = bullets.iterator();it.hasNext();){
			Projectile b = (Projectile) it.next();
			if (b!=null)
				b.draw(canvas);  	
		}
		
		for(Iterator<Ufo> it = ufos.iterator();it.hasNext();){
			Ufo a = (Ufo) it.next();
			if (a!=null)
				a.draw(canvas);  	
		}
		
		tank.draw(canvas);
		hud.draw(canvas);
	}

	/**
	 * Register the images and the objects.
	 */
	public void createGraphics() {
		mBackgroundImage=Bitmap.createBitmap(mBackgroundImage);
		tank = new Tank();
		hud = new Hud();
		hud.register(tank);
	}
	
	/**
	 * Update collisions and firing.
	 * 
	 * @param timer The time before a new UFO spawns
	 */
	private void updatePhysics(long timer) {
		if (isFiring && tank.mode != 0) {
			tank.pressFire();
		}
		timer=(long) (timer);
		tank.update(timer);
		Projectile b ;
		int i=0;
		while (!bullets.isEmpty() && bullets.size()>i){
			b = bullets.get(i);
			if (b.isOver())
				bullets.remove(i);
			else {
				b.update(timer);
				i++;
			}
		}
		
		Ufo a ;
		i=0;
		while (!ufos.isEmpty() && ufos.size()>i){
			a = ufos.get(i);
			if (a.isOver())
				ufos.remove(i);
			else {
				a.update(timer);
				int j=0;
				while (j<bullets.size()){
					b = bullets.get(j);
					if (a.isFlying() && b.isFlying())
						if(a.impactDetected(b)){
							a.setImpact(b.getDamageValue());
							b.setImpact();
							if(a.isOver()){
								hud.addImpact();
								ufos.add(new Ufo());
							}
						}    					
					j++;
				}
				i++;
			}    		
		} 	
		newUfoTimer+=timer;
		if (newUfoTimer>3000){
			ufos.add(new Ufo());
			newUfoTimer=0;
		}
	}
	
	/**
	 * Receive the inputs.
	 * 
	 * @param input The input received
	 */
	public void feedInput(InputObject input) {
		synchronized(inputQueueMutex) {
			try {
				inputQueue.put(input);
			} catch (InterruptedException e) {
			}
		}
	}
	
	/**
	 * Process received inputs.
	 */
	private void processInput() {
		synchronized(inputQueueMutex) {
			ArrayBlockingQueue<InputObject> inputQueue = this.inputQueue;
			while (!inputQueue.isEmpty()) {
				try {
					InputObject input = inputQueue.take();
					if (input.eventType == InputObject.EVENT_TYPE_TOUCH) {
						processMotionEvent(input);
					}
					input.returnToPool();
				} catch (InterruptedException e) {
				}
			}
		}
	}
	
	/**
	 * Manage the screen gestures.
	 * 
	 * @param input The input received
	 */
	private void processMotionEvent(InputObject input) {
		if( input.action==InputObject.ACTION_TOUCH_DOWN){
			if (input.x > Hud.BUTTON_X0 
			 && input.x < Hud.BUTTON_X1
			 && input.y > Hud.BUTTON_Y0
			 && input.y < Hud.BUTTON_Y1) {
				if (tank.mode == 0) {
					tank.mode = 1;
				} else if (tank.mode == 1) {
					tank.mode = 0;
				}
			}
			if (tank.mode == 0) {
				tank.pressFire();
			}
			tank.setTarget(input.x, input.y);
			isFiring = true;
		}
		if( input.action==InputObject.ACTION_TOUCH_UP){
			tank.setTarget(input.x, input.y);
			tank.releaseFire();
			isFiring = false;
		}
		if( input.action==InputObject.ACTION_TOUCH_MOVE)
			tank.setTarget(input.x, input.y);
	}
	
	/**
	 * Create a bullet on the screen.
	 * 
	 * @param angle The angle at which to shoot the bullet
	 * @param power The speed of the bullet
	 * @param x The starting X position
	 * @param y The starting Y position
	 */
	public static void shootBullet(float angle, int power,int x, int y) {
		System.out.println("Cannon");
		bullets.add(new Bullet(power, angle, x, y));
		SoundManager.playShoot();
	}
	
	/**
	 * Create a laser on the screen.
	 * 
	 * @param angle The angle at which to shoot the laser
	 * @param power The speed of the laser
	 * @param x The starting X position
	 * @param y The starting Y position
	 */
	public static void shootLaser(float angle, int power,int x, int y) {
		System.out.println("Laser");
		bullets.add(new Laser(power, angle, x, y));
		SoundManager.playShoot();
	}
}