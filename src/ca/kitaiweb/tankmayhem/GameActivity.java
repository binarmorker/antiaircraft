package ca.kitaiweb.tankmayhem;

import java.util.concurrent.ArrayBlockingQueue;

import ca.kitaiweb.tankmayhem.R;
import android.app.Activity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

/**
 * The main game activity for Tank Mayhem.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class GameActivity extends Activity{

	private GameThread mGameThread;
	public static final int INPUT_QUEUE_SIZE = 30;
	public ArrayBlockingQueue<InputObject> inputObjectPool;
	
	@Override
	public void onStart() {
		super.onStart();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);
		mGameThread = ((GameView) findViewById(R.id.game)).getThread();
		mGameThread.createGraphics();
		createInputObjectPool();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_HOME) {
			mGameThread.setRunning(false);
			mGameThread.onPause();
		}
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mGameThread.setRunning(false);
			mGameThread.onPause();
			this.finish();
			try {
				mGameThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			mGameThread.onResume();
			mGameThread.setRunning(true);
		}
	}
	
	/**
	 * Create the pool of inputs
	 */
	private void createInputObjectPool() {
		inputObjectPool = new ArrayBlockingQueue<InputObject>(INPUT_QUEUE_SIZE);
		for (int i = 0; i < INPUT_QUEUE_SIZE; i++) {
			inputObjectPool.add(new InputObject(inputObjectPool));
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		try {
			int hist = event.getHistorySize();
			for (int i = 0; i < hist; i++) {
				InputObject input = inputObjectPool.take();
				input.useEventHistory(event, i);
				mGameThread.feedInput(input);
			}
			InputObject input = inputObjectPool.take();
			input.useEvent(event);
			mGameThread.feedInput(input);
		} catch (InterruptedException e) {
		}
		try {
			Thread.sleep(16);
		} catch (InterruptedException e) {
		}
		return true;
	}
}