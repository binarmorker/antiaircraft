package ca.kitaiweb.tankmayhem;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * A bullet is created when a Tank is shooting.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class Bullet implements Projectile {
	private static final int STATUS_FLYING = 0;
	private static final int STATUS_BOOM = 1;
	private static final int STATUS_OVER = 2;
	private static final long TIME_EXPLODING  = 100;
	private static final float RADIUS = 5;
	private final int DAMAGE_VALUE = 50;
	private long time;
	private int status;
	private float posX0;
	private float posY0;
	private float posX;
	private float posY;
	private float iniSpeedX;
	private float iniSpeedY;
	private Paint paint;
	private long explodingTimer;

	/**
	 * The constructor for a bullet.
	 * 
	 * @param power The speed of the bullet
	 * @param angle The angle at which the bullet is shot
	 * @param x0 The origin X position
	 * @param y0 The origin Y position
	 */
	public Bullet(int power,float angle,int x0,int y0){
		time=0;
		paint = new Paint();
		posX0 = x0;
		posY0 = y0;
		iniSpeedY=(float) (Math.sin(angle)*power*1.5) ;
		iniSpeedX=(float) (Math.cos(angle)*power*1.5);
	}

	/**
	 * Draw the bullet on the screen.
	 * 
	 * @param c The canvas on which to draw the bullet
	 */
	public void draw(Canvas c) {
		paint.setColor(Color.RED);
		if (status == STATUS_FLYING)
			paint.setColor(Color.BLACK);
		c.drawCircle(posX, posY, RADIUS, paint);
	}

	/**
	 * Update the bullet in space.
	 * 
	 * @param elapsedTime The time elapsed used to calculate the bullet's position
	 */
	public void update(long elapsedTime) {
		switch (status){		
		case STATUS_FLYING:
			time+=elapsedTime;
			double t2=(double)(time)/100;
			posX=(float) (posX0 + iniSpeedX*t2);
			posY=(float) (posY0 - iniSpeedY*t2);
			posY=(float) (posY0 - (iniSpeedY*t2 - (9.8 *Math.pow(t2, 2)/2)));

			if (posY>=480) {
				status=STATUS_BOOM;
				posY = 480;
				explodingTimer = 0;
			}
			break;
		case STATUS_BOOM:
			explodingTimer+=elapsedTime;
			if (explodingTimer>TIME_EXPLODING)
				status=STATUS_OVER;
			break;
		}
	}

	/**
	 * Set the bullet as exploded.
	 */
	public void setImpact(){
		status = STATUS_BOOM;
		explodingTimer=0;
	}

	/**
	 * Get the X position of the bullet.
	 * 
	 * @return The X position
	 */
	public float getPosX() {
		return posX;
	}

	/**
	 * Get the Y position of the bullet.
	 * 
	 * @return The Y position
	 */
	public float getPosY() {
		return posY;
	}

	/**
	 * Check if the bullet is in the air.
	 * 
	 * @return True if the bullet is flying
	 */
	public boolean isFlying(){
		return (status == STATUS_FLYING);
	}

	/**
	 * Check if the bullet is exploding.
	 * 
	 * @return True if the bullet explodes
	 */
	public boolean isExploding() {
		return (status==STATUS_BOOM);
	}

	/**
	 * Check if the bullet is already exploded and should be removed.
	 * 
	 * @return True if the bullet has finished exploding
	 */
	public boolean isOver() {
		return (status==STATUS_OVER);
	}
	
	/**
	 * Get the damage value of the bullet.
	 * 
	 * @return The damage value
	 */
	public int getDamageValue() {
		return DAMAGE_VALUE;
	}

}