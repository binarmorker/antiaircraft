package ca.kitaiweb.tankmayhem;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

/**
 * The tank controller by the player.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class Tank {
	private static final int TANK_HEIGHT = 50;
	private static final int TANK_WIDTH = 50;
	private static final int TANK_TOP = 480 - TANK_HEIGHT;
	private static final int CANNON_WIDTH = 10;
	private static final int GUNBARREL_LENGTH = 46;
	private static final int MODE_CANNON = 0;
	private static final int MODE_LASER = 1;
	private static final int STATUS_IDLE=0;
	private static final int STATUS_POWERING=1;
	private static final long POWERING_TIMER_LIMIT = 1200;
	private float angle;
	public static int cannonEndX;
	public static int cannonEndY;
	public int mode;
	private int status;
	private long poweringTimer;
	private int power;
	private int lastBulletPower;

	/**
	 * The constructor for the tank, which loads images to the RAM.
	 */
	public Tank(){
		GameThread.tankImg=Bitmap.createBitmap(GameThread.tankImg);
		GameThread.cannonImg=Bitmap.createBitmap(GameThread.cannonImg);
	}

	/**
	 * Draw the tank on the screen.
	 * 
	 * @param c THe canvas on which to draw the tank
	 */
	public void draw(Canvas c) {
		c.drawBitmap(GameThread.tankImg, (320-TANK_WIDTH)/2, TANK_TOP, null);	
		Matrix m = new Matrix();
		m.postTranslate((320-CANNON_WIDTH)/2, TANK_TOP - 30);
		m.postRotate((float) ((-angle)*180 /Math.PI),160,446);
		c.drawBitmap(GameThread.cannonImg, m, null);
		cannonEndX = (int) (160 + (Math.cos(angle) * GUNBARREL_LENGTH));
		cannonEndY = (int) (446 - (Math.sin(angle) * GUNBARREL_LENGTH));
	}
	
	/**
	 * Set the angle at which the cannon should point.
	 * 
	 * @param x The X position of the finger
	 * @param y The Y position of the finger
	 */
	public void setTarget(int x, int y) {
		float previousAngle=angle;
		if (x==160)
			angle=0;
		else{
			if(y>=446){
				angle=(float)Math.PI/2;
				if(x>160)
					angle-=2*angle;
			}
			else
				angle = (float) Math.atan((float)(x-160)/(float)(y-446));
		}
		if (Math.abs( (int) ((previousAngle-angle)*180/Math.PI))>0) 
			SoundManager.playMovegun();
	}
	
	/**
	 * Update the status of the tank.
	 * 
	 * @param elapsedTime The time elapsed
	 */
	public void update(long elapsedTime){
		if (mode == MODE_CANNON && status == STATUS_POWERING){
			poweringTimer=poweringTimer+elapsedTime;
			if (poweringTimer>POWERING_TIMER_LIMIT)
				poweringTimer=POWERING_TIMER_LIMIT;
			power = (int) (((float)poweringTimer / POWERING_TIMER_LIMIT) *100);
		}
	}
	
	/**
	 * Fires the laser or charges the cannon.
	 */
	public void pressFire( ) {
		if (mode == MODE_CANNON) {
			status = STATUS_POWERING;
			power = 0;		
			poweringTimer = 0;
		} else if(mode == MODE_LASER) {
			GameThread.shootLaser(angle+(float)Math.PI/2,20+power*60/100,160,446);
		}
	}

	/**
	 * Fires the cannon.
	 */
	public void releaseFire() {
		if (mode == MODE_CANNON) {
			GameThread.shootBullet(angle+(float)Math.PI/2,20+power*60/100,160,446);
			lastBulletPower = power;
			status = STATUS_IDLE;
			power = 0;
		}
	}

	/**
	 * Get the power of the cannon.
	 * 
	 * @return The power
	 */
	public int getPower() {
		return power;
	}

	/**
	 * Get the angle of the cannon.
	 * 
	 * @return The angle
	 */
	public float getAngle() {
		return angle;
	}

	/**
	 * Get the power of the last fired bullet.
	 * 
	 * @return The last bullet's power
	 */
	public int getLastBulletPower() {
		return lastBulletPower;
	}
}