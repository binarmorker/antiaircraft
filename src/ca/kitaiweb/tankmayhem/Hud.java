package ca.kitaiweb.tankmayhem;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Shader;

/**
 * The Hud showing information on the game.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class Hud {
	private static final int BAR_POWER_LEFT_MARGIN = 10;
	private static final int BAR_POWER_RIGHT_MARGIN = 200;
	private static final int BAR_POWER_BOTTOM_MARGIN = 10;
	private static final int BAR_POWER_HEIGHT = 10;
	private static final int TEXT_INFO_LEFT_MARGIN = 10;
	private static final int TEXT_ANGLE_TOP_MARGIN = 25;
	private static final int TEXT_POWER_TOP_MARGIN = 50;
	private static final int TEXT_COUNTER_TOP_MARGIN = 75;
	private static final int TEXT_SIZE = 20;
	public static final float BUTTON_X0 = 10F;
	public static final float BUTTON_Y0 = 420F;
	public static final float BUTTON_X1 = 60F;
	public static final float BUTTON_Y1 = 470F;
	private float barPwrLeft;
	private float barPwrTop;
	private float barPwrRight;
	private float barPwrBottom;
	private int impactCounter;
	LinearGradient gradient;
	Paint paint;
	private Tank tank;

	/**
	 * The constructor for the Hud.
	 */
	public Hud(){
		paint = new Paint();
		barPwrLeft = BAR_POWER_LEFT_MARGIN;
		barPwrTop = 480 - BAR_POWER_BOTTOM_MARGIN - BAR_POWER_HEIGHT;
		barPwrRight = 320 - BAR_POWER_RIGHT_MARGIN;
		barPwrBottom = 480 - BAR_POWER_HEIGHT;
		impactCounter = 0;
		gradient= new LinearGradient (barPwrLeft, barPwrTop, barPwrRight, barPwrBottom, new int[]{Color.GREEN,Color.YELLOW, Color.RED},null, Shader.TileMode.CLAMP);
	}

	/**
	 * Draw the Hud on the screen.
	 * 
	 * @param c The canvas on which to draw the Hud
	 */
	public void draw(Canvas c){
		/*int progress = ((int) (barPwrLeft + tank.getPower() * (barPwrRight-barPwrLeft) /100));
		paint.setAlpha(255);
		paint.setShader(gradient);
		paint.setStyle(Style.FILL);
		c.drawRoundRect(new RectF(barPwrLeft, barPwrTop, progress, barPwrBottom), 4, 4, paint);*/
		paint.setShader(null);
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.BLACK);
		paint.setStrokeWidth(0);
		c.drawRoundRect(new RectF(BUTTON_X0, BUTTON_Y0, BUTTON_X1, BUTTON_Y1), 0, 0, paint);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(0);
		paint.setAntiAlias(true);
		paint.setTextSize(TEXT_SIZE);
		int powerInfo=(tank.getPower()==0?tank.getLastBulletPower():tank.getPower());
		c.drawText("Angle : "+Math.abs((int) (tank.getAngle()*180/Math.PI))+"�", TEXT_INFO_LEFT_MARGIN, TEXT_ANGLE_TOP_MARGIN, paint);
		c.drawText("Power: "+powerInfo, TEXT_INFO_LEFT_MARGIN, TEXT_POWER_TOP_MARGIN, paint);
		c.drawText("Kills : "+impactCounter, TEXT_INFO_LEFT_MARGIN, TEXT_COUNTER_TOP_MARGIN, paint);
	}

	/**
	 * Register the tank to the Hud.
	 * 
	 * @param tank The tank object to register
	 */
	public void register(Tank tank) {
		this.tank = tank;
	}
	
	/**
	 * Add 1 to the impact count.
	 */
	public void addImpact() {
		impactCounter++;
	}
}