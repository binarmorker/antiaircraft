package ca.kitaiweb.tankmayhem;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * A laser is created when a Tank is shooting.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class Laser implements Projectile {
	private static final int STATUS_FLYING = 0;
	private static final int STATUS_BOOM = 1;
	private static final int STATUS_OVER = 2;
	private static final long TIME_EXPLODING  = 100;
	private static final float RADIUS = 3;
	private final int DAMAGE_VALUE = 10;
	private long time;
	private int status;
	private float posX0;
	private float posY0;
	private float posX;
	private float posY;
	private float iniSpeedX;
	private float iniSpeedY;
	private Paint paint;
	private long explodingTimer;

	/**
	 * The constructor for a laser.
	 * 
	 * @param power The speed of the laser
	 * @param angle The angle at which the laser is shot
	 * @param x0 The origin X position
	 * @param y0 The origin Y position
	 */
	public Laser(int power,float angle,int x0,int y0){
		time=0;
		paint = new Paint();
		posX0 = x0;
		posY0 = y0;
		iniSpeedY=(float) (Math.sin(angle)*power*1.5) ;
		iniSpeedX=(float) (Math.cos(angle)*power*1.5);
	}

	/**
	 * Draw the laser on the screen.
	 * 
	 * @param c The canvas on which to draw the laser
	 */
	public void draw(Canvas c) {
		paint.setColor(Color.RED);
		if (status == STATUS_FLYING)
			paint.setColor(Color.CYAN);
		c.drawCircle(posX, posY, RADIUS, paint);
	}

	/**
	 * Update the laser in space.
	 * 
	 * @param elapsedTime The time elapsed used to calculate the laser's position
	 */
	public void update(long elapsedTime) {
		switch (status){		
		case STATUS_FLYING:
			time+=elapsedTime;
			double t2=(double)(time)/100;
			posX=(float) (posX0 + iniSpeedX*t2);
			posY=(float) (posY0 - iniSpeedY*t2);

			if (posY>=480) {
				status=STATUS_BOOM;
				posY = 480;
				explodingTimer = 0;
			}
			break;
		case STATUS_BOOM:
			explodingTimer+=elapsedTime;
			if (explodingTimer>TIME_EXPLODING)
				status=STATUS_OVER;
			break;
		}
	}

	/**
	 * Set the laser as exploded.
	 */
	public void setImpact(){
		status = STATUS_BOOM;
		explodingTimer=0;
	}

	/**
	 * Get the X position of the laser.
	 * 
	 * @return The X position
	 */
	public float getPosX() {
		return posX;
	}

	/**
	 * Get the Y position of the laser.
	 * 
	 * @return The Y position
	 */
	public float getPosY() {
		return posY;
	}

	/**
	 * Check if the laser is in the air.
	 * 
	 * @return True if the laser is flying
	 */
	public boolean isFlying(){
		return (status == STATUS_FLYING);
	}

	/**
	 * Check if the laser is exploding.
	 * 
	 * @return True if the laser explodes
	 */
	public boolean isExploding() {
		return (status==STATUS_BOOM);
	}

	/**
	 * Check if the laser is already exploded and should be removed.
	 * 
	 * @return True if the laser has finished exploding
	 */
	public boolean isOver() {
		return (status==STATUS_OVER);
	}

	/**
	 * Get the damage value of the bullet.
	 * 
	 * @return The damage value
	 */
	public int getDamageValue() {
		return DAMAGE_VALUE;
	}
	
}