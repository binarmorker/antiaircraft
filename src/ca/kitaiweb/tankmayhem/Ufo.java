package ca.kitaiweb.tankmayhem;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;

/**
 * The Ufos spawned as enemies.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class Ufo {
	private static final int STATUS_FLYING = 0;
	private static final int STATUS_OVER = 1;
	private static final float TIME_FLYING = 5;

	private float lowerPosition;
	private long time;
	private int status;
	private float posX;
	private float posY;
	private double iniSpeedX;
	private double iniSpeedY;
	private float posX0;
	private float drawX;
	private float drawY;
	//private int angle;
	private int direction;
	private static float acceleration;
	private int hitBoxHeight;
	private int hitBoxWidth;
	private int life;
	private Paint paint;

	/**
	 * The constructor for the Ufo, which loads resources in the RAM.
	 */
	public Ufo(){
		time = 0;
		//angle = 0;
		life = 50;
		status = STATUS_FLYING;
		direction = (Math.random()<0.5?1:-1);
		posX0 = (float) (160 + (320*-direction*Math.random()));
		
		paint = new Paint();

		lowerPosition = 480;

		acceleration = (float) (lowerPosition/Math.pow(TIME_FLYING/2,2));

		iniSpeedX = (320 / TIME_FLYING);
		iniSpeedY = (TIME_FLYING * acceleration / 2) - 1 / TIME_FLYING;

		GameThread.ufoImg = Bitmap.createBitmap(GameThread.ufoImg);
		hitBoxHeight = GameThread.ufoImg.getHeight();
		hitBoxWidth = GameThread.ufoImg.getWidth();
	}

	/**
	 * Draw the Ufo on the screen.
	 * 
	 * @param c The canvas on which to draw the Ufo.
	 */
	public void draw(Canvas c) {
		Matrix m = new Matrix();
		m.postTranslate(drawX, drawY);

		paint.setColorFilter(new PorterDuffColorFilter(Color.argb(100, life*4, life*4, life*4), Mode.SRC_IN));

		if (status == STATUS_FLYING) {
			c.drawBitmap(GameThread.ufoImg, m, null);
			c.drawBitmap(GameThread.ufoImg, m, paint);
		}
	}

	/**
	 * Update the Ufo status.
	 * 
	 * @param elapsedTime The elapsed time
	 */
	public void update(long elapsedTime) {
		double t2;
		switch (status){		
			case STATUS_FLYING:
				time+=elapsedTime;
				t2=(double)(time)/1000;
				posX = (float) (posX0 +  iniSpeedX*t2*direction);
				posY = (float) (iniSpeedY * t2 - (acceleration/2 * Math.pow(t2,2) ));
				//angle = (direction>0?180:0) + (int) (Math.atan((iniSpeedY-acceleration*t2)/iniSpeedX*direction)*180/Math.PI);
				if (posY<0)
					status=STATUS_OVER;
			break;
		}

		drawX = posX - 30;
		drawY = posY - 20;
	}

	/**
	 * Check if the Ufo should be removed from the game.
	 * 
	 * @return True if the Ufo is dead
	 */
	public boolean isOver() {
		return (status==STATUS_OVER);
	}

	/**
	 * Check if the Ufo is in the air.
	 * 
	 * @return True if the Ufo is flying
	 */
	public boolean isFlying(){
		return (status == STATUS_FLYING);
	}
	
	/**
	 * Check if the Ufo and a bullet are colliding.
	 * 
	 * @param b The bullet to check
	 * @return True if there is a collision
	 */
	public boolean impactDetected(Projectile b) {
		boolean impact = false;
		float diffX=Math.abs(posX-b.getPosX());
		float diffY=Math.abs(posY-b.getPosY());
		if (diffX<hitBoxWidth/2 && diffY<hitBoxHeight/2){
			impact = true;
			SoundManager.playExplode();
		}		
		return impact;
	}
	
	/**
	 * Register an impact and makes the Ufo take damage.
	 */
	public void setImpact(int damage){
		if(life > 0)
			life -= damage;
		else
			status = STATUS_OVER;
	}
}