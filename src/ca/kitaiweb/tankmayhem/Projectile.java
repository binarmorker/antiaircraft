package ca.kitaiweb.tankmayhem;

import android.graphics.Canvas;

public interface Projectile {

	public void draw(Canvas c);
	
	public void update(long elapsedTime);
	
	public void setImpact();
	
	public float getPosX();
	
	public float getPosY();
	
	public boolean isFlying();
	
	public boolean isExploding();
	
	public boolean isOver();
	
	public int getDamageValue();
	
}
