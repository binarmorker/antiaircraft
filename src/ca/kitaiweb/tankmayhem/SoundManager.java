package ca.kitaiweb.tankmayhem;

import ca.kitaiweb.tankmayhem.R;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

/**
 * The sound manager to play music and sounds.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class SoundManager {

	private static SoundPool sounds;
	private static int shoot;
	private static int explode;
	private static int movegun;
	private static boolean movegunPlaying ;
	private static long movegunTimer;
	private static MediaPlayer musicTheme;
	
	/**
	 * Load all audio resources to RAM.
	 * 
	 * @param context The Android context
	 */
	public static void loadSound(Context context) {
		movegunPlaying = false;
		sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		shoot = sounds.load(context, R.raw.baby_crying, 1);
		explode = sounds.load(context, R.raw.open_door, 1);
		movegun = sounds.load(context, R.raw.movegun, 1);
		musicTheme = MediaPlayer.create(context, R.raw.musique_douce);
	}

	/**
	 * Play the background music.
	 */
	public static final void playMusicTheme() {
		if (!musicTheme.isPlaying()) {
			musicTheme.seekTo(0);
			musicTheme.start();
		}
	}

	/**
	 * Pause the background music.
	 */
	public static final void pauseMusic() {
		if (musicTheme.isPlaying()) musicTheme.pause();
	}
	
	/**
	 * Resume the background music where it was paused.
	 */
	public static final void resumeMusic() {
		if (!musicTheme.isPlaying()) musicTheme.start();
	}

	/**
	 * Play the shoot sound.
	 */
	public static void playShoot() {
		sounds.play(shoot, 0.5F, 0.5F, 1, 0, 1);
	}

	/**
	 * Play the explosion sound.
	 */
	public static void playExplode() {
		sounds.play(explode, 0.5F, 0.5F, 1, 0, 1);
	}

	/**
	 * Play the moving sound.
	 */
	public static void playMovegun() {
		if (!movegunPlaying){
			sounds.play(movegun, 0.5F, 0.5F, 1, 0, 1);
			movegunPlaying = true;
			movegunTimer=0;
		}
	}

	/**
	 * Update the music timer.
	 * 
	 * @param time The time elapsed
	 */
	public static void update(long time){
		playMusicTheme();
		if (movegunPlaying){
			movegunTimer+=time;
			if (movegunTimer>302){
				movegunPlaying=false;
				movegunTimer=0;
			}
		}
	}
}