package ca.kitaiweb.tankmayhem;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * The game view is what draws the thread on the screen.
 * 
 * @author Fran�ois Allard & Marc-Antoine Renaud
 *
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {
	private GameThread thread;

	/**
	 * The constructor for the game view.
	 * 
	 * @param context The android context
	 * @param attrs Variables from the Android activity
	 */
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		thread = new GameThread(holder, context,null);
	}

	/**
	 * Getter for the game thread.
	 * 
	 * @return The thread
	 */
	public GameThread getThread() {
		return thread;
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {}
}